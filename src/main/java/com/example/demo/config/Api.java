package com.example.demo.config;

/**
 * Created by nhatle on 2/1/18.
 */
public interface Api {
    interface Event {
        String CREATE = "/event";
        String GET = "/event/{id}";
        String DELETE = "/event/{id}";
        String UPDATE = "/event";
        String LIST = "/events";
        String DELETES = "/events";
        String UPDATES = "/events";
        String SEARCH = "/event/search";
    }

    interface New {
        String DELETE = "/news/{id}";
        String LIST = "/news";
    }
}
