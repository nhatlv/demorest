package com.example.demo.config;

/**
 * Created by nhatle on 2/1/18.
 */
public interface Version {
    String V1 = "/api/v1";
    String V2 = "/api/v2";
}
