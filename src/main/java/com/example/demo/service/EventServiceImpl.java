package com.example.demo.service;

import com.example.demo.common.Message;
import com.example.demo.exception.EventNotFoundException;
import com.example.demo.model.Event;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

/**
 * Created by nhatle on 2/1/18.
 */
@Service
public class EventServiceImpl implements EventService {

    @Override
    public Event getEvent(int id) throws Exception {

        if (404 == id) throw new EventNotFoundException(String.format(Message.NOT_FOUND_EVENT, id));

        if (500 == id) throw new Exception("Internal");

        return new Event(id, "test");
    }

    @Override
    public int delete(int id) {
        return 0;
    }

    @Override
    public List<Event> getALL() {
        return Arrays.asList(new Event(1, "Event1"), new Event(2, "Event2"));
    }

    @Override
    public int delete() {
        return 100;
    }
}
