package com.example.demo.service;

import com.example.demo.model.Event;

import java.util.List;

/**
 * Created by nhatle on 2/1/18.
 */
public interface EventService {
    Event getEvent(int id) throws Exception;

    int delete(int id);

    List<Event> getALL();

    int delete();
}
