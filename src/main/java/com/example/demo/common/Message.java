package com.example.demo.common;

/**
 * Created by nhatle on 2/1/18.
 */
public class Message {
    public static final String INTERNAL_SERVER_ERROR = "Unexpected error";
    public static final String ARGUMENT_TYPE_MISMATCH_ERROR = "Request required parameter '%s' with type '%s'";
    public static final String MISSING_REQUEST_PARAMETER_ERROR = "Request requires parameter '%s'";
    public static final String AUTHENTICATE_FAIL = "Authorization fail";
    public static final String NOT_FOUND_EVENT = "Event '%s' not found";
}
