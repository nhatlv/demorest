package com.example.demo.model;

/**
 * Created by nhatle on 2/1/18.
 */
public class ErrorResponse extends BaseResponse {

    private final String error;

    public ErrorResponse(String message) {
        super(false);
        this.error = message;
    }

    public String getError() {
        return this.error;
    }
}