package com.example.demo.model;

/**
 * Created by nhatle on 2/1/18.
 */
public class SuccessResponse<T> extends BaseResponse {
    private final T data;

    public SuccessResponse() {
        super(true);
        this.data = null;
    }

    public SuccessResponse(T data) {
        super(true);
        this.data = data;
    }

    public T getData() {
        return this.data;
    }
}
