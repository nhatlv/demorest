package com.example.demo.model;

/**
 * Created by nhatle on 2/1/18.
 */
public abstract class ServiceException extends Exception {
    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }
}
