package com.example.demo.model;

/**
 * Created by nhatle on 2/1/18.
 */
public abstract class BaseResponse {

    private final boolean success;

    public BaseResponse(boolean success) {
        this.success = success;
    }

    public boolean isSuccess() {
        return this.success;
    }
}
