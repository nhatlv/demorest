package com.example.demo.exception;

import com.example.demo.model.BaseResponse;
import com.example.demo.model.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * Created by nhatle on 2/1/18.
 */
public class ServiceExceptionHandler extends BaseExceptionHandler {
    /**
     * Handles not found Event, return status 404.
     *
     * @param exception EventNotFoundException
     * @return ResponseEntity
     */
    @ExceptionHandler(EventNotFoundException.class)
    public ResponseEntity<BaseResponse> handleException(EventNotFoundException exception) {
        return new ResponseEntity<>(new ErrorResponse(exception.getMessage()), HttpStatus.NOT_FOUND);
    }


    /**
     * Handles UnAuthorized request, return status 403.
     *
     * @param exception EventNotFoundException
     * @return ResponseEntity
     */
    @ExceptionHandler(UnAuthorizedRequestException.class)
    public ResponseEntity<BaseResponse> handleException(UnAuthorizedRequestException exception) {
        return new ResponseEntity<>(new ErrorResponse(exception.getMessage()), HttpStatus.FORBIDDEN);
    }
}
