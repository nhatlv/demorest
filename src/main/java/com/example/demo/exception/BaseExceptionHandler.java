package com.example.demo.exception;

import com.example.demo.common.Message;
import com.example.demo.model.BaseResponse;
import com.example.demo.model.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

/**
 * Created by nhatle on 2/1/18.
 */
public class BaseExceptionHandler {

    /**
     * Handles unknown (unhandled by other handlers) exceptions which are the most probably internal errors (500).
     *
     * @param exception Exception
     * @return ResponseEntity
     */
    @ExceptionHandler(Exception.class)
    public ResponseEntity<BaseResponse> handleException(Exception exception) {
        return new ResponseEntity<>(new ErrorResponse(Message.INTERNAL_SERVER_ERROR), HttpStatus.INTERNAL_SERVER_ERROR);
    }


    /**
     * Handles exceptions which are wrong type require. ie : parameter require int but input string
     *
     * @param exception : MethodArgumentTypeMismatchException
     * @return ResponseEntity
     */
    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<BaseResponse> handleException(MethodArgumentTypeMismatchException exception) {
        return new ResponseEntity<>(
                new ErrorResponse(String.format(Message.ARGUMENT_TYPE_MISMATCH_ERROR, exception.getName(), exception.getRequiredType().getName()))
                , HttpStatus.BAD_REQUEST
        );
    }

    /**
     * Handles exceptions which missing require parameters
     *
     * @param exception : MissingServletRequestParameterException
     * @return ResponseEntity
     */
    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ResponseEntity<BaseResponse> handleException(MissingServletRequestParameterException exception) {
        return new ResponseEntity<>(
                new ErrorResponse(String.format(Message.MISSING_REQUEST_PARAMETER_ERROR, exception.getParameterName())),
                HttpStatus.BAD_REQUEST
        );
    }
}
