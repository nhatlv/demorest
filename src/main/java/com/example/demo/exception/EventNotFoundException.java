package com.example.demo.exception;

import com.example.demo.model.ServiceException;

/**
 * Created by nhatle on 2/1/18.
 */
public class EventNotFoundException extends ServiceException {
    public EventNotFoundException(String message) {
        super(message);
    }

    public EventNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
