package com.example.demo.exception;

import com.example.demo.model.ServiceException;

/**
 * Created by nhatle on 2/1/18.
 */
public class UnAuthorizedRequestException extends ServiceException {
    public UnAuthorizedRequestException(String message) {
        super(message);
    }

    public UnAuthorizedRequestException(String message, Throwable cause) {
        super(message, cause);
    }
}
