package com.example.demo.controller;

import com.example.demo.common.Message;
import com.example.demo.exception.ServiceExceptionHandler;
import com.example.demo.exception.UnAuthorizedRequestException;
import org.springframework.beans.factory.annotation.Value;

/**
 * Created by nhatle on 2/1/18.
 */
public class BaseController extends ServiceExceptionHandler{

    @Value("${event.authorization.code:vobQ5JPkj7tuehIfP3Fo}")
    private String AUTHORIZATION_CODE;

    public void basicAuthorized(String basicToken) throws UnAuthorizedRequestException {
        if (!AUTHORIZATION_CODE.equals(basicToken)) throw new UnAuthorizedRequestException(Message.AUTHENTICATE_FAIL);
    }
}
