package com.example.demo.controller;

import com.example.demo.config.Api;
import com.example.demo.config.Version;
import com.example.demo.model.BaseResponse;
import com.example.demo.model.Event;
import com.example.demo.model.SuccessResponse;
import com.example.demo.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by nhatle on 2/1/18.
 */
@RestController
@RequestMapping(Version.V1)
public class EventController extends BaseController {

    @Autowired
    EventService eventService;

    @PostMapping(value = Api.Event.CREATE, produces = "application/json; charset=utf-8")
    private BaseResponse create(@RequestBody Event event) {
        return new SuccessResponse<>(event);
    }

    @GetMapping(value = Api.Event.LIST)
    private BaseResponse get() throws Exception {
        return new SuccessResponse<>(eventService.getALL());
    }

    @DeleteMapping(value = Api.Event.DELETES)
    private BaseResponse deletes() throws Exception {
        return new SuccessResponse<>(eventService.delete());
    }

    @PutMapping(value = Api.Event.UPDATES, produces = "application/json; charset=utf-8")
    private BaseResponse updates(@RequestBody List<Event> events) throws Exception {
        return new SuccessResponse<>(events);
    }

    @GetMapping(value = Api.Event.SEARCH, produces = "application/json; charset=utf-8")
    private BaseResponse search(@RequestParam(value = "name", defaultValue = "") String name, @RequestParam(value = "limit", defaultValue = "1", required = false) int limit) throws Exception {
        return new SuccessResponse<>(eventService.getALL().stream().filter(event -> name.equalsIgnoreCase(event.getName())).limit(limit).collect(Collectors.toList()));
    }

    @PutMapping(value = Api.Event.UPDATE, produces = "application/json; charset=utf-8")
    private BaseResponse update(@PathVariable(name = "id") int id, @RequestBody Event event) {
        return new SuccessResponse<>(event);
    }

    @GetMapping(value = Api.Event.GET)
    private BaseResponse get(@PathVariable(name = "id") int id) throws Exception {
        return new SuccessResponse<>(eventService.getEvent(id));
    }


    @DeleteMapping(value = Api.Event.DELETE)
    private BaseResponse delete(@PathVariable(name = "id") int id, @RequestHeader(name = "Authorization") String basicToken) throws Exception {
        basicAuthorized(basicToken);
        return new SuccessResponse<>(eventService.delete(id));
    }
}
